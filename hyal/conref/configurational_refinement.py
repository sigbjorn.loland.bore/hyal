import warnings
from pathlib import Path
from typing import Any, Callable, List, Optional, Union

from hyobj import DataSet


class ConRef():
    """Active learning a la Sigbjorn."""

    def __init__(self,
                 *args,
                 calculator: Optional[Callable] = None,
                 sampler: Optional[Callable] = None,
                 ml_engine: Optional[Callable] = None):

        self.calculator = calculator
        self.sampler = sampler
        self.ml_engine = ml_engine

        self.calculator_output_parser = (self.
                                         calculator.
                                         OutputParser)  # type: ignore
        self.calculator_input_parser = (self.
                                        calculator.
                                        InputParser)  # type: ignore

    def extract_configs(self, samples: Any):
        """Extract configs from sampling."""
        if isinstance(samples, dict):
            return self.extract_configs_lammps(samples)
        else:
            raise TypeError('currently only lammps sampling allowed')

    def extract_configs_lammps(self, samples: dict):
        """Extract configs from LAMMPS sampling."""
        # from LAMMPS output
        clen = 6
        cslice = slice(0, clen)
        a = samples.get('atoms', [])
        atoms = [e for e in [a] for _ in range(clen)]
        c = samples.get('trajectories', [])
        coordinates = c[cslice]
        b = samples.get('boxes', [])
        boxes = b[cslice]
        cdict = {'atoms': atoms, 'coord': coordinates, 'box': boxes}
        return DataSet(cdict, dimension='multi')

    def gen_configs(self, models: Any, samples: Any):
        """Generate configurations."""
        configs = self.extract_configs(samples)
        # for m in models:
        #     # print('model', m)
        #     result = self.ml_engine.infer(model=m, data=configs)
        #     energy = result['energy']
        #     force  = result['force']
        #     virial = result['virial']
        #     for c in configs:
        #         xyz = c['coords']
        #         atoms = c['atoms']
        #         box = c['box']
        #     #  do something...

        return configs

    def get_initial_set(self, initial_set):
        """Get initial training set."""
        if not initial_set:
            raise ValueError('initial set needed')

        if isinstance(initial_set, DataSet):
            return initial_set
        elif isinstance(initial_set, (str, Path)):
            return self.get_initial_set_from_file(str(initial_set))

    def get_initial_set_from_file(self, initial_set):
        """Get initial set from file."""
        path = Path(initial_set)

        if path.is_dir():
            return self.get_initial_set_from_dir(path)

        return DataSet(path)

    def get_initial_set_from_dir(self, path: Path) -> Union[Path, DataSet]:
        """Get initial set from dir.

        Parameter
        ---------
        path : pathlib.Path
            path to directory

        Returns
        -------
        Union[Path, DataSet]
            if 'type_map.raw' and 'type.raw' are found in path,
            path is returned.
            Else, a DataSet with parsed output results is returned.

        """
        files: List[str] = [f.name for f in sorted(path.iterdir())]
        if all(['type_map.raw' in files, 'type.raw' in files]):
            return path

        input_suffix = self.calculator.__dict__.get('input_file_suffix',
                                                    '.inp')
        output_suffix = self.calculator.__dict__.get('output_file_suffix',
                                                     '.out')

        not_calculated: list = []
        num_input = 0
        num_output = 0
        ffiles: List[Path] = sorted(path.iterdir())
        for f in ffiles:
            if f.suffix == input_suffix:
                num_input += 1
                if not f.with_suffix(output_suffix) in ffiles:
                    not_calculated.append(f.name)
            elif f.suffix == output_suffix:
                num_output += 1

        if num_input + num_input <= 0:
            raise ValueError(f'could not find either input {input_suffix}' +
                             f'nor output files {output_suffix}')

        if len(not_calculated) > 0:
            w = 'the following input files dont have a corresponding output:\n'
            w += '\n'.join(not_calculated)
            warnings.warn(w)

        if not hasattr(self.calculator, 'OutputParser'):
            raise AttributeError('calculator should contain an OutputParser' +
                                 'please contact ' +
                                 f'{self.calculator.author}')  # type: ignore
        parser = self.calculator.OutputParser  # type: ignore
        match = '*' + output_suffix
        ds_out = DataSet(path, match=match, parser=parser)

        if not hasattr(self.calculator, 'InputParser'):
            raise AttributeError('calculator should contain an InputParser ' +
                                 'please contact ' +
                                 f'{self.calculator.author}')  # type: ignore

        parser = self.calculator.InputParser  # type: ignore
        match = '*' + input_suffix
        ds_inp = DataSet(path, match=match, parser=parser)

        ds = ds_inp + ds_out

        # nan_rows = ds.data[ds.data.isna().any(axis=1)]

        # if not nan_rows.empty:
        #     w = f'Data is not complete \n {nan_rows}'
        #     warnings.warn(w)

        forces_list = ['gradient', 'gradients', 'force', 'forces']
        coord_list = ['coordinates', 'coords', 'xyz']
        box_list = ['box']
        energy_list = ['energy', 'en']

        cols = list(ds.data.columns)

        if not any([f in cols for f in forces_list]):
            warnings.warn('could not found forces in DataSet')

        if not any([f in cols for f in coord_list]):
            warnings.warn('could not found coordinates in DataSet')

        if not any([f in cols for f in box_list]):
            warnings.warn('could not found box in DataSet')

        if not any([f in cols for f in energy_list]):
            warnings.warn('could not found energy in DataSet')

        return ds
