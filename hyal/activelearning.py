from copy import deepcopy
from typing import Any, Callable, Dict, Optional, Union

import numpy as np
from hyif import HylleraasMLInterface
from hyobj import DataSet, Molecule

from .conref import ConRef

CONF_GENS: Dict[str, Callable] = {'ConRef': ConRef}


class ActiveLearning:
    """Hylleraas Active Learning class."""

    def __init__(self,
                 *args,
                 ml_engine: Optional[Callable] = None,
                 calculator: Optional[Callable] = None,
                 sampler: Optional[Callable] = None,
                 initial_set: Optional[Any] = None,
                 variant: Optional[str] = 'ConRef') -> None:
        self.calculator: Callable = calculator  # type: ignore
        self.sampler: Callable = sampler  # type: ignore
        self.ml_engine: HylleraasMLInterface = ml_engine
        variant = str(variant)

        self.config_generator = CONF_GENS[variant](calculator=self.calculator,
                                                   sampler=self.sampler,
                                                   ml_engine=self.ml_engine)

        if initial_set is not None:
            self.initial_set = self.generate_initial_set(initial_set)

    def generate_initial_set(self, initial_set: Any) -> DataSet:
        """Generate initial set for training models."""
        return self.config_generator.get_initial_set(initial_set)

    def gen_model_from_descriptor(self, descriptor: dict) -> dict:
        """Generate model from descriptor."""
        return self.ml_engine.gen_model_from_descriptor(descriptor)

    def run(
        self,
        initial_set: Optional[Any] = None,
        model_descriptors: Optional[Union[dict, list]] = [],
        maxiter: Optional[int] = 10,
        options: dict = {},
    ):
        """Perform an active learning model optimization."""
        model_opts: list = self.generate_model_options(model_descriptors)
        if initial_set is None:
            data = self.initial_set
        else:
            data = self.generate_initial_set(initial_set)

        for i in range(maxiter):  # type: ignore
            train = self.train_models(model_opts, data)
            models = [train[j]['model'] for j in range(len(train))]
    # * print(f'alternative perform simulation with {self.sampler}')<-LAMMPS

            # arbitrarily use first molecule with this box
            sample_start = Molecule(data[0])
            sample_start._properties = {
                    'unit_cell': [20, 0, 0, 0, 20, 0, 0, 0, 20]
                                        }
            samples = self.sampler.run(sample_start,  # type: ignore
                                       {'model': models[0]})
            #  print('refinement/filter/...')<- CALLABLE
            confs = self.config_generator.gen_configs(models, samples)
            print('configurations:\n', confs)
    # * print(f'compute model deviation among models with {self.ml_engine}')
            dev = self.get_deviation(models=models, reference=confs)
            print('model deviation:\n', dev)
            outliers = self.get_outliers(confs, dev[:, -3])

    # * print(f'process candidates/filter/refinement... possibly iteratively')

            if len(outliers) == 0:
                break

    # * print(f'append configuration', dataset -> Molecule)

            for c in outliers:
                result = self.calculator.run(Molecule(c))  # type: ignore
                ds = DataSet(result)
                data = data + ds

        if i < maxiter:  # type: ignore
            print(f'active learning loop converged in {i} iterations')

        return models

    def prepare_objs_for_calculator(self, outliers: list,
                                    reference: dict) -> list:
        """Prepare data, e.g. Molecule for calculator."""
        # check the input type of the calculator, default is Molecule
        calc_obj = self.calculator.__dict__.get('object', Molecule)
        objs = []
        if isinstance(calc_obj, Molecule):
            t = reference['system']['type_raw']
            tm = reference['system']['type_map_raw']
            atoms = [tm[ti] for ti in t]
        for ol in outliers:

            if isinstance(calc_obj, Molecule):
                coord = reference['trajectories'][ol]
                obj = Molecule({'atoms': atoms, 'coordinates': coord})

            objs.append(obj)
        return objs

    def get_outliers(self, confs: DataSet, vals: list,
                     thresh_min: Optional[float] = 1e-8,
                     thresh_max: Optional[float] = 1e-4):
        """Get outliers."""
        # print('maximal outlier', np.amax(vals))
        # TODO: configurational refinement
        idx: list = []
        for i in range(len(vals)):
            if vals[i] > thresh_min and vals[i] < thresh_max:
                idx.append(i)

        if len(idx) == 0:
            return []

        outliers = confs[idx[0]]
        for i in range(1, len(idx)):
            ds = outliers + confs[idx[i]]
            outliers = ds

        return outliers

    def generate_model_options(self,
                               model_descriptors: Any
                               ) -> list:
        """Generate models for active learning."""
        return [dict(d) for d in model_descriptors]
        # TODO: move to ML interface
        # return [self.gen_model_from_descriptor(d) for d in model_descriptors]

    def train_models(self, models: list, data: DataSet) -> list:
        """Train models."""
        results: list = []
        for m in models:
            result: dict = {}
            result_train = self.ml_engine.train(data, options=m)
            model = result_train['model']

            result_test = self.ml_engine.test(model)
            result['model'] = model
            result['train_results'] = result_train
            result['test_results'] = result_test
            results.append(result)

        return results

    def merge_dicts(self, d1: dict, d2: dict) -> dict:
        """Merge dictionaries."""
        d: dict = {}
        print()
        if d1 == {}:
            return d2
        for k, v1 in d1.items():
            v2 = d2[k]
            if isinstance(v1, list):
                v1.append(v2)
            elif isinstance(v1, np.ndarray):
                np.append(v1, np.array(v2))
            elif isinstance(v1, dict):
                v1.update(v2)
            else:
                v1 = [v1, v2]

            d[k] = v1
        return d

    def gen_reference(self,
                      models: Optional[list] = [],
                      parameters: Optional[list] = [],
                      options: Optional[dict] = {}) -> dict:
        """Generate reference for computing model deviation."""
        results: list = []
        for m in models:  # type: ignore
            for p in parameters:  # type: ignore
                popt = deepcopy(options)
                popt['parameters'] = p  # type: ignore
                result = self.ml_engine.predict(model=m, options=popt)
                results.append(result)

        combined: dict = {}
        for r in results:
            combined = self.merge_dicts(combined, r)

        return combined

    def get_deviation(self, models: list, reference: Any):
        """Get model deviation."""
        return self.ml_engine.get_model_deviation(models=models,
                                                  reference=reference)

        # model_predict = models[-1]['train_results']['model']
        # predict = self.ml_engine.predict(model=model_predict,@
        # options=options)
        # predictions = self.ml_engine.combine_predictions([predict])
        # models = [ m['train_results']['model'] for m in models]
        # deviation = self.ml_engine.get_model_deviation
        # (models =models, reference=predictions)
        # print('f_max\n',deviation[:,-3])
        # return models
