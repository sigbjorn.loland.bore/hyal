import hyif
import hyset

from hyal import ActiveLearning

wdir = '/root/scratch/'
myenv = hyset.create_compute_settings('local', work_dir=wdir, debug=True)
myenv_xtb = hyset.create_compute_settings('local',
                                          work_dir=wdir, debug=True,
                                          launcher='conda run -n xtb')
myenv_lammps = hyset.create_compute_settings('local',
                                             work_dir=wdir, debug=True,
                                             launcher='conda run -n lammps')
myenv_deepmd = hyset.create_compute_settings(
    'local', work_dir=wdir, debug=True,
    launcher=['conda', 'run', '-n', 'deepmd'])


def test_sigbjorn():
    """Testing active learning a la Sigbjørn."""
    myxtb = hyif.Xtb({}, compute_settings=myenv_xtb)
    mylammps = hyif.Lammps({}, compute_settings=myenv_lammps)
    mydeepmd = hyif.DeepMD({'check_version': False},
                           compute_settings=myenv_deepmd)
    initial_set = '/root/outs'

    al = ActiveLearning(
        calculator=myxtb,
        ml_engine=mydeepmd,
        sampler=mylammps,
        variant='ConRef',
    )

    models = al.run(initial_set=initial_set,
                    model_descriptors=[{
                        'seed': 1
                    }, {
                        'seed': 9
                    }])
    print('optimized models:\n', models)


if __name__ == '__main__':
    test_sigbjorn()
